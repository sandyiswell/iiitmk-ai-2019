import random
import logging
from midsem import agent


class Board:
    def __init__(self, f1, f2):
        self.board = [[" ", " ", " "], [" ", " ", " "], [" ", " ", " "]]
        self.p1 = "x"
        self.p2 = "o"
        self.f1 = f1
        self.f2 = f2
        self.current = (self.p1, self.f1)

    def valid_moves(self):
        return [
            (x, y)
            for x, row in enumerate(self.render())
            for y, cell in enumerate(row)
            if cell == " "
        ]

    def render(self):
        return tuple(["".join(i) for i in self.board])

    def valid_move(self, coords):
        x, y = coords
        return self.board[x][y] == " "

    def who_won(self):
        B = self.board
        for line in [
            [[0, 0], [0, 1], [0, 2]],  # row1
            [[1, 0], [1, 1], [1, 2]],  # row2
            [[2, 0], [2, 1], [2, 2]],  # row3
            [[0, 0], [1, 0], [2, 0]],  # col1
            [[0, 1], [1, 1], [2, 1]],  # col2
            [[0, 2], [1, 2], [2, 2]],  # col3
            [[0, 0], [1, 1], [2, 2]],  # diag
            [[0, 2], [1, 1], [2, 0]],  # diag
        ]:
            if all([B[i][j] == self.p1 for i, j in line]):
                return (self.p1, self.f1)
            if all([B[i][j] == self.p2 for i, j in line]):
                return (self.p2, self.f2)

    def move(self, coords):
        x, y = coords
        symbol, _ = self.current
        assert self.board[x][y] == " ", "Invalid move"
        assert symbol == self.p1 or symbol == self.p2, "Invalid symbol"
        self.board[x][y] = symbol
        # flip current player
        self.current = (self.p1, self.f1) if symbol == self.p2 else (self.p2, self.f2)


def random_agent(board, symbol):
    valid = [
        (x, y)
        for x, row in enumerate(board)
        for y, cell in enumerate(row)
        if cell == " "
    ]
    return random.choice(valid)


def show(start, end, out, fn, symbol):
    name = str(fn)
    arrow = f"--[{name:<45}]--[put {symbol} at {out}]-->"
    pad = " " * len(arrow)
    print(f"|{start[0]}|{pad}|{end[0]}|")
    print(f"|{start[1]}|{arrow}|{end[1]}|")
    print(f"|{start[2]}|{pad}|{end[2]}|")
    print(f"|___|{pad}|___|")


won_games = 0
total_games = 100
for nth_game in range(total_games):
    try:
        print(f"{nth_game} Game")
        a = random_agent if nth_game % 2 == 0 else agent
        b = agent if nth_game % 2 == 0 else random_agent
        print(a, "is starting player")
        board = Board(a, b)
        while True:
            symbol, fn = board.current
            start = board.render()
            out = fn(board.render(), symbol)
            assert len(out) == 2, f"Please return two coordinates. Got: {out}"
            assert all(
                [isinstance(i, int) for i in out]
            ), f"Please make sure returned coordinates are integers. Got: {out}"
            assert (
                tuple(out) in board.valid_moves()
            ), f"{out} not in valid moves {board.valid_moves()}"
            board.move(out)
            end = board.render()
            show(start, end, out, fn, symbol)
            win = board.who_won()
            if win is not None:
                p, f = win
                print(f, "won the game")
                if f == agent:
                    won_games += 1
                break
            if win is None and len(board.valid_moves()) == 0:
                print("Game was drawn.")
                break
        print("-" * 50)
    except Exception as e:
        logging.exception(e)
with open("metrics.txt", "a") as fl:
    fl.write(f"Case1 {won_games}/{total_games}\n")
