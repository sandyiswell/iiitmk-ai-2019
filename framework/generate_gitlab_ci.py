import os

print(
    """stages:
    - setup
    - assignment

folders_are_ok:
    image: 'python:3.6'
    before_script:
        - pip install pipenv
        - cd framework
        - pipenv install --dev --deploy
    script:
        - pipenv run python generate_gitlab_ci.py > gitlab-reference
        - diff gitlab-reference ../.gitlab-ci.yml
        - pipenv run black --check ..
    stage: setup
    """
)


ci_job = """
{jobname}_mock_midsem:
    image: 'python:3.6'
    variables:
        NAME: '{name}'
    before_script:
        - echo $NAME
        - cd "people/$NAME"
        - cp ../../framework/mockmidsem/case1.py .
        - cp ../../framework/mockmidsem/case2.py .
    script:
        - ls
        - python case1.py
        - python case2.py
    stage: assignment
    rules:
        - exists:
            - people/{name}/mock_midsem.py

{jobname}_midsem:
    image: 'python:3.6'
    variables:
        NAME: '{name}'
    before_script:
        - echo $NAME
        - cd "people/$NAME"
        - "touch metrics.txt"
        - cp ../../framework/midsem/case1.py .
        - cp ../../framework/midsem/case2.py .
    script:
        - ls
        - python case1.py
        - python case2.py
        - mv metrics.txt ../..
        - cd ../..
        - ls
        - cat metrics.txt
    stage: assignment
    artifacts:
        reports:
            metrics: metrics.txt
    rules:
        - exists:
            - people/{name}/midsem.py


{jobname}:
    image: 'python:3.6'
    variables:
        NAME: '{name}'
    before_script:
        - echo $NAME
        - cd "people/$NAME"
        - touch dummy.py
        - pip install gym numpy
    script:
        - python ../../framework/checker.py
    stage: assignment
    coverage: '/AssignmentsDone: \d+/'
"""

for name in sorted(os.listdir("../people")):
    jobname = name.lower()
    print(ci_job.format(jobname=jobname, name=name))

for world in sorted(os.listdir("worlds")):
    print(
        f"""
world_{world.replace('.', '')}:
    image: 'python:3.6'
    variables:
        WORLD: 'framework/worlds/{world}'
    before_script:
        - pip install pipenv
        - echo $WORLD
        - pipenv install --dev --deploy
    script:
        - pipenv run python framework/simulation.py "$WORLD"
    stage: assignment
    coverage: '/AssignmentsDone: \d+/'
    """
    )
